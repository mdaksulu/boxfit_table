//
// Created by Mehmet Deniz Aksulu on 27/03/2020.
//

#include "sharpness_generator.h"
sharpness_generator::sharpness_generator(char* scalefit_table) {
  if (myid == 0) {
    scalefit_sharpness = new H5::H5File(scalefit_table, H5F_ACC_RDWR);
    printf("Opened %s\n.", scalefit_table);
  }
}
void sharpness_generator::generate_table() {
  int nu_obs_res = 50;
  int p_res;
  double *t_obs;
  double theta_0;
  double *E;
  double *n_ref;
  double theta_obs;
  double *p;
  double *epsilon_B;
  double *epsilon_e;
  double *xi_N;

  double *nu_a;
  double *nu_a2;
  double *nu_a3;
  double *nu_a3alt;
  double *nu_a4;
  double *nu_a5;
  double *nu_m;
  double *nu_c;
  double *f_peak;

  string spec_types[6] = {"/spec_1", "/spec_2", "/spec_3", "/spec_3alt", "/spec_4", "/spec_5"};
  for (int k = 0; k < 6; k++) {
    string spec_id;
    // Spectrum 1
    spec_id = spec_types[k];
    read_table(&p_res, (spec_id + "/p_res").c_str());
    read_table(&theta_0, (spec_id + "/theta_0").c_str());
    read_table(&theta_obs, (spec_id + "/theta_obs").c_str());

    if (myid == 0) {
      printf("Calculating sharpness for spectrum 1.\n");
      printf("p_res = %d.\n", p_res);
      printf("theta_0 = %10e.\n", theta_0);
      printf("theta_obs = %10e.\n", theta_obs);
    }
    MPI_Bcast(&p_res, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&theta_0, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&theta_obs, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&xi_N, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    t_obs = new double[p_res];
    E = new double[p_res];
    n_ref = new double[p_res];
    p = new double[p_res];
    epsilon_B = new double[p_res];
    epsilon_e = new double[p_res];
    xi_N = new double[p_res];
    nu_a = new double[p_res];
    nu_a2 = new double[p_res];
    nu_a3 = new double[p_res];
    nu_a3alt = new double[p_res];
    nu_a4 = new double[p_res];
    nu_a5 = new double[p_res];
    nu_m = new double[p_res];
    nu_c = new double[p_res];
    f_peak = new double[p_res];

    read_table(t_obs, (spec_id + "/t_obs").c_str());
    read_table(E, (spec_id + "/E").c_str());
    read_table(n_ref, (spec_id + "/n_ref").c_str());
    read_table(p, (spec_id + "/p").c_str());
    read_table(epsilon_B, (spec_id + "/epsilon_B").c_str());
    read_table(epsilon_e, (spec_id + "/epsilon_e").c_str());
    read_table(xi_N, (spec_id + "/xi_N").c_str());
    read_table(nu_a, (spec_id + "/nu_a").c_str());
    read_table(nu_a2, (spec_id + "/nu_a2").c_str());
    read_table(nu_a3, (spec_id + "/nu_a3").c_str());
    read_table(nu_a3alt, (spec_id + "/nu_a3alt").c_str());
    read_table(nu_a4, (spec_id + "/nu_a4").c_str());
    read_table(nu_a5, (spec_id + "/nu_a5").c_str());
    read_table(nu_m, (spec_id + "/nu_m").c_str());
    read_table(nu_c, (spec_id + "/nu_c").c_str());
    read_table(f_peak, (spec_id + "/f_peak").c_str());

    MPI_Bcast(t_obs, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(E, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(n_ref, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(p, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(epsilon_B, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(epsilon_e, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(xi_N, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(nu_a, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(nu_a2, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(nu_a3, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(nu_a3alt, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(nu_a4, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(nu_a5, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(nu_m, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(nu_c, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(f_peak, p_res, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    hsize_t dims[2];
    dims[0] = p_res;
    dims[1] = nu_obs_res;
    int total_size = nu_obs_res * p_res;
    double f_nu[total_size];
    double nu_obs[total_size];
    for (int i = 0; i < p_res; i++) {
      double nu_breaks[8] = {nu_a[i], nu_m[i], nu_c[i], nu_a2[i], nu_a3[i], nu_a3alt[i], nu_a4[i], nu_a5[i]};
      double nu_min = nu_breaks[0];
      double nu_max = nu_breaks[0];
      for (int i = 1; i < 8; i++) {
        if (nu_breaks[i] < nu_min) nu_min = nu_breaks[i];
        if (nu_breaks[i] > nu_max) nu_max = nu_breaks[i];
      }
      nu_min = nu_min / 1e3;
      nu_max = nu_max * 1e3;
      double nu_obs_current[nu_obs_res];
      double f_obs_current[nu_obs_res];
      for (int j = 0; j < nu_obs_res; j++) {
        int current_index = i * nu_obs_res + j;
        nu_obs[current_index] = pow(10, log10(nu_min) + j * (log10(nu_max) - log10(nu_min)) / (nu_obs_res - 1));
        nu_obs_current[j] = nu_obs[current_index];
      }

      if (myid == 0) {
        printf("(%s) Generating spectrum for p = %10e (%10e to %10e Hz). %d/%d completed.\n", spec_id.c_str(), p[i], nu_min, nu_max, i, p_res);
      }

      double parameters[8] = {theta_0, E[i], n_ref[i], theta_obs, p[i], epsilon_B[i], epsilon_e[i], xi_N[i]};
      generate_f_nu(parameters, t_obs[i], nu_obs_current, f_obs_current, nu_obs_res);
      for (int j = 0; j < nu_obs_res; j++) {
        int current_index = i * nu_obs_res + j;
        f_nu[current_index] = f_obs_current[j];
        if (myid == 0) {
//        printf("\t\t%d: %10e, %10e\n", j, nu_obs[current_index], f_nu[current_index]);
        }
      }
    }
    write_table(f_nu, (spec_id + "/f_nu").c_str(), 2, dims);
    write_table(nu_obs, (spec_id + "/nu_obs").c_str(), 2, dims);
  }

  if (myid == 0) {
    scalefit_sharpness->close();
  }
}
void sharpness_generator::generate_f_nu(double *param, double t_obs, double *nu_obs, double *f_nu, int no_points) {
  double fitvars[no_fit_vars_];
  fitvars[fit_theta_0_] = param[0];
  fitvars[fit_E_] = log10(param[1]);
  fitvars[fit_n_] = log10(param[2]);
  fitvars[fit_theta_obs_] = param[3];
  fitvars[fit_p_] = param[4];
  fitvars[fit_epsilon_B_] = log10(param[5]);;
  fitvars[fit_epsilon_E_] = log10(param[6]);
  fitvars[fit_ksi_N_] = log10(param[7]);
  int data_count = no_points;

  int i;
  int idle_id;
  MPI_Status err;
  int stopsignal = -1;

  double calculated_flux_temp[data_count]; // temp buffer to collect results

  // clean out Ftemp and Fresult
  for (i = 0; i < data_count; i++) calculated_flux_temp[i] = 0.0;

  prepare_wrapper(fitvars);
  if (myid == 0) {
    for (i = 0; i < data_count; i++) {
      // wait for a proc signalling that it is idle
      MPI_Recv(&idle_id, 1, MPI_INT, MPI_ANY_SOURCE, 10, MPI_COMM_WORLD, &err);

      // send the number of the datapoint to this machine
      MPI_Ssend(&i, 1, MPI_INT, idle_id, 20, MPI_COMM_WORLD);
    }

    // tell cores that are idle to stop signaling and expecting signals
    // by sending them a stop signal -1 instead of a datapoint number
    for (i = 1; i < numprocs; i++) {
      // wait for a proc signalling that it is idle
      MPI_Recv(&idle_id, 1, MPI_INT, MPI_ANY_SOURCE, 10, MPI_COMM_WORLD, &err);

      // send special signal (-1) telling it to stop
      MPI_Ssend(&stopsignal, 1, MPI_INT, idle_id, 20, MPI_COMM_WORLD);
    }
  } else // any nonzero proc
  {
    for (;;) // infinite loop -until broken
    {
      // tell proc 0 that your ID is available for computations
      MPI_Ssend(&myid, 1, MPI_INT, 0, 10, MPI_COMM_WORLD);

      // receive the number of the datapoint to start working on
      MPI_Recv(&i, 1, MPI_INT, 0, 20, MPI_COMM_WORLD, &err);

      if (i >= 0) {
        calculated_flux_temp[i] = calculate_flux_wrapper(t_obs, nu_obs[i]);
      } else {
        break;
      }
    }
  }

  // We now have an array Ftemp at each processor, filled with zeroes
  // except, for each processor, the ones that particular processor has
  // calculated. We collect the sums of all individual entries at myid = 0,
  // with for each datapoint only a single processor contributing a nonzero
  // term.
  MPI_Barrier(MPI_COMM_WORLD); // wait until all cores have caught up
  MPI_Allreduce(&calculated_flux_temp[0], &f_nu[0], data_count, MPI_DOUBLE, MPI_SUM,
                MPI_COMM_WORLD);
  MPI_Barrier(MPI_COMM_WORLD); // wait until all cores have caught up
}

void sharpness_generator::write_table(double *_data, std::string _name, int _rank, hsize_t *_dim) {
  if (myid == 0) {
    H5::DataSpace *dataSpace;
    H5::DataSet dataSet;
    H5::IntType *dataType;

    dataType = new H5::IntType(H5::PredType::NATIVE_DOUBLE);
    dataSpace = new H5::DataSpace(_rank, _dim);
    dataSet = scalefit_sharpness->createDataSet(_name.c_str(), *dataType, *dataSpace);
    dataSet.write(_data, H5::PredType::NATIVE_DOUBLE);
    delete dataType;
    delete dataSpace;
  }
}
void sharpness_generator::write_table(int *_data, std::string _name, int _rank, hsize_t *_dim) {
  if (myid == 0) {
    H5::DataSpace *dataSpace;
    H5::DataSet dataSet;
    H5::IntType *dataType;

    dataType = new H5::IntType(H5::PredType::NATIVE_INT);
    dataSpace = new H5::DataSpace(_rank, _dim);
    dataSet = scalefit_sharpness->createDataSet(_name.c_str(), *dataType, *dataSpace);
    dataSet.write(_data, H5::PredType::NATIVE_INT);
    delete dataType;
    delete dataSpace;
  }
}

void sharpness_generator::read_table(double *_data, std::string _name) {
  if (myid == 0) {
    H5::DataSet dataSet;

    dataSet = scalefit_sharpness->openDataSet(_name.c_str());
    dataSet.read(_data, H5::PredType::NATIVE_DOUBLE);
  }
}
void sharpness_generator::read_table(int *_data, std::string _name) {
  if (myid == 0) {
    H5::DataSet dataSet;

    dataSet = scalefit_sharpness->openDataSet(_name.c_str());
    dataSet.read(_data, H5::PredType::NATIVE_INT);
  }
}