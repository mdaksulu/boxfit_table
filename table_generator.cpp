//
// Created by Mehmet Deniz Aksulu on 13/09/2019.
//

#include "table_generator.h"

table_generator::table_generator(table_properties _properties) {
  properties = _properties;

  theta_0 = new double[properties.theta_0_res];
  double log10_theta_0_min = log10(properties.theta_0_min);
  double log10_theta_0_max = log10(properties.theta_0_max);
  double d_log10_theta_0 = (log10_theta_0_max - log10_theta_0_min) / (properties.theta_0_res - 1);
  for (int i = 0; i < properties.theta_0_res; i++) theta_0[i] = pow(10, log10_theta_0_min + i * d_log10_theta_0);

  times = new double[properties.time_res];
  double log10_times_min = log10(properties.time_min);
  double log10_times_max = log10(properties.time_max);
  double d_log10_times = (log10_times_max - log10_times_min) / (properties.time_res - 1);
  for (int i = 0; i < properties.time_res; i++) times[i] = pow(10, log10_times_min + i * d_log10_times);

  theta_obs = new double[properties.theta_obs_res];
  for (int i = 0; i < properties.theta_obs_res; i++)
    theta_obs[i] = properties.theta_obs_min
        + i * (properties.theta_obs_max - properties.theta_obs_min) / properties.theta_obs_res;

  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
}
table_generator::~table_generator() {
  delete[] times;
  delete[] theta_obs;
}

void table_generator::generate_table() {
  auto start = chrono::steady_clock::now();
  if (myid == 0) outfile = new H5::H5File(properties.filename.c_str(), H5F_ACC_TRUNC);
  hsize_t _dim = 1;
  write_table(&(properties.E_k_iso), "E_iso", 1, &_dim);
  write_table(&(properties.d_L), "d_L", 1, &_dim);
  write_table(&(properties.epsilon_B), "epsilon_B", 1, &_dim);
  write_table(&(properties.epsilon_E), "epsilon_e", 1, &_dim);
  write_table(&(properties.k), "k", 1, &_dim);
  write_table(&(properties.n_ref), "n_ref", 1, &_dim);
  write_table(&(properties.p), "p_synch", 1, &_dim);
  write_table(&(properties.snapshots_res), "snapshots_res", 1, &_dim);
  write_table(&(properties.theta_0_res), "theta_0_res", 1, &_dim);
  write_table(&(properties.theta_obs_res), "theta_obs_res", 1, &_dim);
  write_table(&(properties.time_res), "times_res", 1, &_dim);
  write_table(&(properties.uphi_rays), "uphi_rays", 1, &_dim);
  write_table(&(properties.ur_rays), "ur_rays", 1, &_dim);
  write_table(&(properties.ksi_N), "xi_N", 1, &_dim);
  write_table(&(properties.z), "z", 1, &_dim);

  write_table(&(properties.nu_hi), "nu_hi", 1, &_dim);
  write_table(&(properties.nu_lo), "nu_lo", 1, &_dim);
  write_table(&(properties.nu_lo_ssa), "nu_lo_ssa", 1, &_dim);

  _dim = properties.time_res;
  write_table(times, "t", 1, &_dim);
  _dim = properties.theta_0_res;
  write_table(theta_0, "theta_0", 1, &_dim);
  _dim = properties.theta_obs_res;
  write_table(theta_obs, "theta_obs", 1, &_dim);

  hsize_t
      _dims[3];
  _dims[0] = properties.time_res;
  _dims[1] = properties.theta_0_res;
  _dims[2] = properties.theta_obs_res;
  double f_hi_temp[properties.time_res];
  double f_hi_c_temp[properties.time_res];
  double f_lo_temp[properties.time_res];
  double f_lo_ssa_temp[properties.time_res];
  unsigned long total_size = properties.time_res * properties.theta_0_res * properties.theta_obs_res;
  double f_hi[total_size];
  double f_hi_c[total_size];
  double f_lo[total_size];
  double f_lo_ssa[total_size];
  double f_peak[total_size];
  double nu_m[total_size];
  double nu_c[total_size];
  double nu_a[total_size];

  for (unsigned long k = 0; k < properties.theta_obs_res; k++) {
    for (unsigned long j = 0; j < properties.theta_0_res; j++) {
      if (properties.on_axis_only) {
        if (k < 1) {
          generate_f_nu(false, false, properties.nu_hi, theta_0[j], theta_obs[k], f_hi_temp);
          generate_f_nu(false, true, properties.nu_hi, theta_0[j], theta_obs[k], f_hi_c_temp);
          generate_f_nu(false, false, properties.nu_lo, theta_0[j], theta_obs[k], f_lo_temp);
          generate_f_nu(true, false, properties.nu_lo_ssa, theta_0[j], theta_obs[k], f_lo_ssa_temp);
        } else {
          for (int i = 0; i < properties.time_res; i++) f_hi_temp[i] = 0;
          for (int i = 0; i < properties.time_res; i++) f_hi_c_temp[i] = 0;
          for (int i = 0; i < properties.time_res; i++) f_lo_temp[i] = 0;
          for (int i = 0; i < properties.time_res; i++) f_lo_ssa_temp[i] = 0;
        }
      } else {
        generate_f_nu(false, false, properties.nu_hi, theta_0[j], theta_obs[k], f_hi_temp);
        generate_f_nu(false, true, properties.nu_hi, theta_0[j], theta_obs[k], f_hi_c_temp);
        generate_f_nu(false, false, properties.nu_lo, theta_0[j], theta_obs[k], f_lo_temp);
        generate_f_nu(true, false, properties.nu_lo_ssa, theta_0[j], theta_obs[k], f_lo_ssa_temp);
      }
      for (unsigned long i = 0; i < properties.time_res; i++) {
        unsigned long
            index_1d = i * properties.theta_0_res * properties.theta_obs_res + j * properties.theta_obs_res + k;
        f_hi[index_1d] = f_hi_temp[i];
        f_hi_c[index_1d] = f_hi_c_temp[i];
        f_lo[index_1d] = f_lo_temp[i];
        f_lo_ssa[index_1d] = f_lo_ssa_temp[i];

        double _nu_m = pow((f_lo_temp[i] / f_hi_temp[i])
                               * (pow(properties.nu_hi, (1 - properties.p) / 2.) / pow(properties.nu_lo, 1. / 3.)),
                           6. / (1 - 3 * properties.p));
        if (std::isnan(_nu_m)) _nu_m = 0;
//        double _f_peak = f_hi_temp[i] * pow(_nu_m / properties.nu_hi, (1 - properties.p) / 2.);
        double _nu_c = pow((f_hi_temp[i] / f_hi_c_temp[i])
                               * (pow(properties.nu_hi, -1 * properties.p / 2.)
                                   / pow(properties.nu_hi, (1 - properties.p) / 2.)),
                           -2.);
        if (std::isnan(_nu_c)) _nu_c = 0;
        double _f_peak = f_lo_temp[i] * pow(_nu_m / properties.nu_lo, 1. / 3.);
        if (std::isnan(_f_peak)) _f_peak = 0;
        double _nu_peak = std::min(_nu_m, _nu_c);

        double _nu_a = pow((_f_peak / f_lo_ssa_temp[i])
                               * (pow(properties.nu_lo_ssa, 2.) / pow(_nu_peak, 1. / 3.)),
                           3. / 5.);
        if (std::isnan(_nu_a)) _nu_a = 0;

        f_peak[index_1d] = _f_peak *
            pow((properties.p - 1) / (3 * properties.p - 1), -1) *
            pow(properties.ksi_N, -1) *
            pow(properties.epsilon_B, -0.5);
        nu_m[index_1d] = _nu_m *
            pow((properties.p - 2) / (properties.p - 1), -2) *
            pow(properties.ksi_N, 2) *
            pow(properties.epsilon_B, -0.5);
        nu_c[index_1d] = _nu_c * pow(properties.epsilon_B, 3. / 2.);
        nu_a[index_1d] = _nu_a *
            pow(properties.ksi_N, -8. / 5.) *
            pow(properties.p - 1, -8. / 5.) *
            (properties.p - 2) *
            pow(properties.p + 2, 3. / 5.) *
            pow(properties.epsilon_B, -1. / 5.);
      }
      if (myid == 0) {
        if (properties.on_axis_only) {
          auto end = chrono::steady_clock::now();
          float current = (properties.theta_0_res * k + j) + 1;
          float total = properties.theta_0_res;
          float seconds = chrono::duration_cast<chrono::seconds>(end - start).count();
          float minutes = seconds / 60;
          printf(
              "%3.4f percent completed. %.4f minutes passed. Estimated completion in %.4f minutes. (On-axis only)\n",
              100. * current / total, minutes, (minutes / current) * (total - current));
          fflush(stdout);
        } else {
          auto end = chrono::steady_clock::now();
          float current = (properties.theta_0_res * k + j) + 1;
          float total = properties.theta_0_res * properties.theta_obs_res;
          float seconds = chrono::duration_cast<chrono::seconds>(end - start).count();
          float minutes = seconds / 60;
          printf(
              "%3.4f percent completed. %.4f minutes passed. Estimated completion in %.4f minutes.\n",
              100. * current / total, minutes, (minutes / current) * (total - current));
          fflush(stdout);
        }
      }
    }
  }
  write_table(f_hi, "Fhi", 3, _dims);
  write_table(f_hi_c, "Fhic", 3, _dims);
  write_table(f_lo, "Flo", 3, _dims);
  write_table(f_lo_ssa, "Flossa", 3, _dims);
  write_table(f_peak, "F_peak", 3, _dims);
  write_table(nu_m, "nu_m", 3, _dims);
  write_table(nu_c, "nu_c", 3, _dims);
  write_table(nu_a, "nu_a", 3, _dims);
  if (myid == 0) outfile->close();
  MPI_Barrier(MPI_COMM_WORLD);
}

void table_generator::generate_f_nu(bool _ssa,
                                    bool _cooling,
                                    double _nu_obs,
                                    double _theta_0,
                                    double _theta_obs,
                                    double *_f_nu) {
#if OPEN_MPI_ == ENABLED_
  double fitvars[no_fit_vars_];
  fitvars[fit_theta_0_] = _theta_0;
  fitvars[fit_E_] = log10(properties.E_k_iso);
  fitvars[fit_n_] = log10(properties.n_ref);
  if (properties.theta_obs_frac) fitvars[fit_theta_obs_] = _theta_0 * _theta_obs;
  else fitvars[fit_theta_obs_] = _theta_obs;
  fitvars[fit_p_] = properties.p;
  if (!_ssa) fitvars[fit_epsilon_B_] = log10(properties.epsilon_B);
  else fitvars[fit_epsilon_B_] = log10(properties.epsilon_B);
  if (!_ssa) fitvars[fit_epsilon_E_] = log10(properties.epsilon_E);
  else fitvars[fit_epsilon_E_] = log10(properties.epsilon_E);
  if (!_ssa) fitvars[fit_ksi_N_] = log10(properties.ksi_N);
  else fitvars[fit_ksi_N_] = log10(properties.ksi_N);

  int data_count = properties.time_res;

  int i;
  int idle_id;
  MPI_Status err;
  int stopsignal = -1;

  double calculated_flux_temp[data_count]; // temp buffer to collect results

  // clean out Ftemp and Fresult
  for (i = 0; i < data_count; i++) calculated_flux_temp[i] = 0.0;

  prepare_wrapper(fitvars, _cooling, _ssa);
  // myid 0 divides the different datapoints over all the processors
  if (myid == 0) {
    for (i = 0; i < data_count; i++) {
      // wait for a proc signalling that it is idle
      MPI_Recv(&idle_id, 1, MPI_INT, MPI_ANY_SOURCE, 10, MPI_COMM_WORLD, &err);

      // send the number of the datapoint to this machine
      MPI_Ssend(&i, 1, MPI_INT, idle_id, 20, MPI_COMM_WORLD);
    }

    // tell cores that are idle to stop signaling and expecting signals
    // by sending them a stop signal -1 instead of a datapoint number
    for (i = 1; i < numprocs; i++) {
      // wait for a proc signalling that it is idle
      MPI_Recv(&idle_id, 1, MPI_INT, MPI_ANY_SOURCE, 10, MPI_COMM_WORLD, &err);

      // send special signal (-1) telling it to stop
      MPI_Ssend(&stopsignal, 1, MPI_INT, idle_id, 20, MPI_COMM_WORLD);
    }
  } else // any nonzero proc
  {
    for (;;) // infinite loop -until broken
    {
      // tell proc 0 that your ID is available for computations
      MPI_Ssend(&myid, 1, MPI_INT, 0, 10, MPI_COMM_WORLD);

      // receive the number of the datapoint to start working on
      MPI_Recv(&i, 1, MPI_INT, 0, 20, MPI_COMM_WORLD, &err);

      if (i >= 0) {
        calculated_flux_temp[i] = calculate_flux_wrapper(times[i], _nu_obs);
      } else {
        break;
      }
    }
  }

  // We now have an array Ftemp at each processor, filled with zeroes
  // except, for each processor, the ones that particular processor has
  // calculated. We collect the sums of all individual entries at myid = 0,
  // with for each datapoint only a single processor contributing a nonzero
  // term.
  MPI_Barrier(MPI_COMM_WORLD); // wait until all cores have caught up
  MPI_Allreduce(&calculated_flux_temp[0], &_f_nu[0], data_count, MPI_DOUBLE, MPI_SUM,
                MPI_COMM_WORLD);
  MPI_Barrier(MPI_COMM_WORLD); // wait until all cores have caught up
//  if (myid == 0) {
//    for (int i = 0; i < no_fit_vars_; i++) {
//      printf("%10e, ", fitvars[i]);
//    }
//    printf("%10e, %s, %s\n", _nu_obs, _ssa ? "true" : "false", _cooling ? "true" : "false");
//  }
#endif
}

double table_generator::generate_f_nu(bool _ssa,
                                      bool _cooling,
                                      double _nu_obs,
                                      double _t_obs,
                                      double _theta_0,
                                      double _theta_obs) {
  double fitvars[no_fit_vars_];
  fitvars[fit_theta_0_] = _theta_0;
  fitvars[fit_E_] = log10(properties.E_k_iso);
  fitvars[fit_n_] = log10(properties.n_ref);
  if (properties.theta_obs_frac) fitvars[fit_theta_obs_] = _theta_0 * _theta_obs;
  else fitvars[fit_theta_obs_] = _theta_obs;
  fitvars[fit_p_] = properties.p;
  if (!_ssa) fitvars[fit_epsilon_B_] = log10(properties.epsilon_B);
  else fitvars[fit_epsilon_B_] = log10(properties.epsilon_B);
  if (!_ssa) fitvars[fit_epsilon_E_] = log10(properties.epsilon_E);
  else fitvars[fit_epsilon_E_] = log10(properties.epsilon_E);
  if (!_ssa) fitvars[fit_ksi_N_] = log10(properties.ksi_N);
  else fitvars[fit_ksi_N_] = log10(properties.ksi_N);

  int data_count = properties.time_res;

  int i;
  int idle_id;
  MPI_Status err;
  int stopsignal = -1;

  prepare_wrapper(fitvars, _cooling, _ssa);
  double result = calculate_flux_wrapper(_t_obs, _nu_obs);
  if (myid == 0) {
    for (int i = 0; i < no_fit_vars_; i++) {
      printf("%10e, ", fitvars[i]);
    }
    printf("\n");
  }
  return result;
}

void table_generator::write_table(double *_data, std::string _name, int _rank, hsize_t *_dim) {
  if (myid == 0) {
    H5::DataSpace *dataSpace;
    H5::DataSet dataSet;
    H5::IntType *dataType;

    dataType = new H5::IntType(H5::PredType::NATIVE_DOUBLE);
    dataSpace = new H5::DataSpace(_rank, _dim);
    dataSet = outfile->createDataSet(_name.c_str(), *dataType, *dataSpace);
    dataSet.write(_data, H5::PredType::NATIVE_DOUBLE);
    delete dataType;
    delete dataSpace;
  }
}
void table_generator::write_table(int *_data, std::string _name, int _rank, hsize_t *_dim) {
  if (myid == 0) {
    H5::DataSpace *dataSpace;
    H5::DataSet dataSet;
    H5::IntType *dataType;

    dataType = new H5::IntType(H5::PredType::NATIVE_INT);
    dataSpace = new H5::DataSpace(_rank, _dim);
    dataSet = outfile->createDataSet(_name.c_str(), *dataType, *dataSpace);
    dataSet.write(_data, H5::PredType::NATIVE_INT);
    delete dataType;
    delete dataSpace;
  }
}