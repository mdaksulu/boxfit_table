//
// Created by Mehmet Deniz Aksulu on 27/03/2020.
//

#ifndef BOXFIT_TABLE__SHARPNESS_GENERATOR_H_
#define BOXFIT_TABLE__SHARPNESS_GENERATOR_H_

#include <H5Cpp.h>
#include <cmath>
#include <string>
#include <mpi.h>
#include "boxfit.h"
#include <chrono>

class sharpness_generator {
 public:
  H5::H5File *scalefit_sharpness;
  H5::H5File *boxfit_sharpness;
  int p_res;
  sharpness_generator(char* scalefit_table);
  void generate_table();
  void generate_f_nu(double* param, double t_obs, double* nu_obs, double *f_nu, int no_points);
  void write_table(double *_data, std::string _name, int _rank, hsize_t *_dim);
  void write_table(int *_data, std::string _name, int _rank, hsize_t *_dim);
  void read_table(double *_data, std::string _name);
  void read_table(int *_data, std::string _name);
};

#endif //BOXFIT_TABLE__SHARPNESS_GENERATOR_H_
