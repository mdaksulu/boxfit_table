//
// Created by Mehmet Deniz Aksulu on 13/09/2019.
//

#ifndef BOXFIT_TABLE__TABLE_GENERATOR_H_
#define BOXFIT_TABLE__TABLE_GENERATOR_H_

#include <H5Cpp.h>
#include <cmath>
#include <string>
#include <mpi.h>
#include "boxfit.h"
#include <chrono>

typedef struct {
  bool on_axis_only = false;
  bool theta_obs_frac = true;

  int theta_0_res = 34;
  double theta_0_min = 0.01;
  double theta_0_max = 1.6;
//  double theta_0[34] =
//      {0.01, 0.02, 0.03, 0.04, 0.045, 0.05, 0.075, 0.1, 0.125, 0.15, 0.175, 0.2, 0.225, 0.25, 0.275, 0.3, 0.325, 0.35,
//       0.375, 0.4, 0.425, 0.45, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6};

  double E_k_iso = 1e52;
  double n_ref = 1;
  double d_L = 1e28;
  double z = 0;

  int theta_obs_res = 100;
  double theta_obs_min = 0.0;
  double theta_obs_max = 10.0;

  double p = 2.5;
  double epsilon_E = 1.0;
  double epsilon_B = 1e-10;
  double ksi_N = 1e-5;

  int time_res = 100;
  double time_min = 1e2;
  double time_max = 1e9;

  int snapshots_res = 250;
  int uphi_rays = 10;
  int ur_rays = 250;
  int k = 2;

  std::string filename = "table.h5";

  double nu_hi = 1e40;
  double nu_lo = 1e-25;
  double nu_lo_ssa = 1e-10;
} table_properties;

class table_generator {
 public:
  table_generator(table_properties _properties);
  void generate_table();
  virtual ~table_generator();
  table_properties properties;
  double *theta_0;
  double *times;
  double *theta_obs;
 private:
  void generate_f_nu(bool _ssa, bool _cooling, double nu_obs, double theta_0, double theta_obs, double *f_nu);
  double generate_f_nu(bool _ssa,
                       bool _cooling,
                       double _nu_obs,
                       double _t_obs,
                       double _theta_0,
                       double _theta_obs);
  void write_table(double *_data, std::string _name, int _rank, hsize_t *_dim);
  void write_table(int *_data, std::string _name, int _rank, hsize_t *_dim);
  H5::H5File *outfile;
  int myid, numprocs;
};

#endif //BOXFIT_TABLE__TABLE_GENERATOR_H_
